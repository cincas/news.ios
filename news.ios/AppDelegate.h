//
//  AppDelegate.h
//  news.ios
//
//  Created by Tyler Yang on 16/2/4.
//  Copyright © 2016年 yangt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


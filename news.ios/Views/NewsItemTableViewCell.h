//
//  NewsItemTableViewCell.h
//  news.ios
//
//  Created by Tyler Yang on 16/2/9.
//  Copyright © 2016年 yangt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsItemTableViewCell : UITableViewCell
@property (nonatomic, weak, readonly) UILabel *titleLabel;
@property (nonatomic, weak, readonly) UIImageView *thumbnailView;
@property (nonatomic, weak, readonly) UILabel *descriptionLabel;

/**
 *  Update constraints to deal with different image sizes
 *
 *  @param width  thumbnail image width
 *  @param height thumbnail image height
 */
- (void)updateThumbnailToWidth:(CGFloat)width height:(CGFloat)height;
@end

//
//  MainViewController.h
//  news.ios
//
//  Created by Tyler Yang on 16/2/4.
//  Copyright © 2016年 yangt. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MainViewModel;
@interface MainViewController : UIViewController
@property (nonatomic, weak) MainViewModel *viewModel;

/**
 *  Initial view controller with view model
 *
 *  @param viewModel MainViewModel
 *
 *  @return instancetype
 */
- (instancetype)initWithViewModel:(MainViewModel *)viewModel;
@end

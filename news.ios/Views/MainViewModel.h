//
//  MainViewModel.h
//  news.ios
//
//  Created by Tyler Yang on 16/2/4.
//  Copyright © 2016年 yangt. All rights reserved.
//

#import "TYViewModel.h"

@class UIViewController, NewsAPIResponseItem;

/**
 *  View model for Main view
 */
@interface MainViewModel : TYViewModel
@property (nonatomic, weak) UIViewController *viewController;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, weak) NSArray <NewsAPIResponseItem *> *items;

/**
 *  Send asynchronized api request
 *
 *  @param complete completion block
 *  @param failed   failed block
 */
- (void)prepareContent:(void (^)())complete failed:(void (^)())failed;
@end

//
//  NewsItemTableViewCell.m
//  news.ios
//
//  Created by Tyler Yang on 16/2/9.
//  Copyright © 2016年 yangt. All rights reserved.
//

#import "NewsItemTableViewCell.h"

const CGFloat kDefaultImageWidth = 100.f;
const CGFloat kDefaultImageHeight = 44.f;
const CGFloat kDefaultPadding = 5.f;

@interface NewsItemTableViewCell ()
@property (nonatomic, weak) NSLayoutConstraint *thumbnailWidthConstraint;
@property (nonatomic, weak) NSLayoutConstraint *thumbnailHeightConstraint;
@end

@implementation NewsItemTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        UILabel *titleLabel = [UILabel new];
        titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline];
        titleLabel.numberOfLines = 0;
        titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [self.contentView addSubview:titleLabel];
        
        UILabel *descriptionLabel = [UILabel new];
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = NO;
        descriptionLabel.textColor = [UIColor darkGrayColor];
        descriptionLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
        descriptionLabel.numberOfLines = 0;
        descriptionLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [self.contentView addSubview:descriptionLabel];
        
        UIImageView *thumbnailView = [UIImageView new];
        thumbnailView.translatesAutoresizingMaskIntoConstraints = NO;
        thumbnailView.contentMode = UIViewContentModeScaleAspectFill;
        thumbnailView.backgroundColor = [UIColor grayColor];
        [self.contentView addSubview:thumbnailView];
        
        _titleLabel = titleLabel;
        _descriptionLabel = descriptionLabel;
        _thumbnailView = thumbnailView;

        self.selectionStyle = UITableViewCellSelectionStyleNone;
        if ([self respondsToSelector:@selector(layoutMargins)]) {
            self.layoutMargins = UIEdgeInsetsZero;
        }
        [self installConstraints];
    }
    return self;
}

#pragma mark - === Autolayout Constraints ===
#pragma mark -
- (void)installConstraints {
    [self installTitleConstraints];
    [self installThumbnailConstraints];
    [self installDescriptionConstraints];
    
    NSLayoutConstraint *bottomConstraint1 = [NSLayoutConstraint constraintWithItem:self.contentView
                                                                         attribute:NSLayoutAttributeBottom
                                                                         relatedBy:NSLayoutRelationGreaterThanOrEqual
                                                                            toItem:self.descriptionLabel
                                                                         attribute:NSLayoutAttributeBottom
                                                                        multiplier:1.0
                                                                          constant:0];
    
    NSLayoutConstraint *bottomConstraint2 = [NSLayoutConstraint constraintWithItem:self.contentView
                                                                         attribute:NSLayoutAttributeBottom
                                                                         relatedBy:NSLayoutRelationGreaterThanOrEqual
                                                                            toItem:self.thumbnailView
                                                                         attribute:NSLayoutAttributeBottom
                                                                        multiplier:1.0
                                                                          constant:0];
    
    [self.contentView addConstraints:@[bottomConstraint1, bottomConstraint2]];
}

- (void)installThumbnailConstraints {
    NSLayoutConstraint *topConstraint = [NSLayoutConstraint constraintWithItem:self.thumbnailView
                                                                      attribute:NSLayoutAttributeTop
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:self.descriptionLabel
                                                                      attribute:NSLayoutAttributeTop
                                                                     multiplier:1.0
                                                                       constant:kDefaultPadding];
 
    NSLayoutConstraint *rightConstraint = [NSLayoutConstraint constraintWithItem:self.thumbnailView
                                                                      attribute:NSLayoutAttributeRight
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:self.contentView
                                                                      attribute:NSLayoutAttributeRight
                                                                     multiplier:1.0
                                                                       constant:-kDefaultPadding];
    
    [self.contentView addConstraints:@[topConstraint, rightConstraint]];
    
    // Default image size constraints
    NSLayoutConstraint *widthConstrant = [NSLayoutConstraint constraintWithItem:self.thumbnailView
                                                                      attribute:NSLayoutAttributeWidth
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:nil
                                                                      attribute:NSLayoutAttributeNotAnAttribute
                                                                     multiplier:1.f
                                                                       constant:kDefaultImageWidth];
    
    NSLayoutConstraint *heightConstrant = [NSLayoutConstraint constraintWithItem:self.thumbnailView
                                                                       attribute:NSLayoutAttributeHeight
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:nil
                                                                       attribute:NSLayoutAttributeNotAnAttribute
                                                                      multiplier:1.f
                                                                        constant:kDefaultImageHeight];
    self.thumbnailWidthConstraint = widthConstrant;
    self.thumbnailHeightConstraint = heightConstrant;
    
    [self.thumbnailView addConstraints:@[widthConstrant, heightConstrant]];
}

- (void)installTitleConstraints {
    NSLayoutConstraint *topConstraint = [NSLayoutConstraint constraintWithItem:self.titleLabel
                                                                     attribute:NSLayoutAttributeTop
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.contentView
                                                                     attribute:NSLayoutAttributeTop
                                                                    multiplier:1.0
                                                                      constant:kDefaultPadding];
    
    NSLayoutConstraint *rightConstraint = [NSLayoutConstraint constraintWithItem:self.titleLabel
                                                                       attribute:NSLayoutAttributeRight
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:self.contentView
                                                                       attribute:NSLayoutAttributeRight
                                                                      multiplier:1.0
                                                                        constant:-kDefaultPadding];
    
    NSLayoutConstraint *leftConstraint = [NSLayoutConstraint constraintWithItem:self.titleLabel
                                                                      attribute:NSLayoutAttributeLeft
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:self.contentView
                                                                      attribute:NSLayoutAttributeLeft
                                                                     multiplier:1.0
                                                                       constant:kDefaultPadding];
    
    [self.contentView addConstraints:@[topConstraint, leftConstraint, rightConstraint]];
}

- (void)installDescriptionConstraints {
    NSLayoutConstraint *topConstraint = [NSLayoutConstraint constraintWithItem:self.descriptionLabel
                                                                     attribute:NSLayoutAttributeTop
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.titleLabel
                                                                     attribute:NSLayoutAttributeBottom
                                                                    multiplier:1.0
                                                                      constant:kDefaultPadding];
    
    NSLayoutConstraint *rightConstraint = [NSLayoutConstraint constraintWithItem:self.descriptionLabel
                                                                       attribute:NSLayoutAttributeRight
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:self.thumbnailView
                                                                       attribute:NSLayoutAttributeLeft
                                                                      multiplier:1.0
                                                                        constant:-kDefaultPadding];
    
    NSLayoutConstraint *leftConstraint = [NSLayoutConstraint constraintWithItem:self.descriptionLabel
                                                                      attribute:NSLayoutAttributeLeft
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:self.contentView
                                                                      attribute:NSLayoutAttributeLeft
                                                                     multiplier:1.0
                                                                       constant:kDefaultPadding];
    
    [self.contentView addConstraints:@[topConstraint, leftConstraint, rightConstraint]];
}

- (void)updateThumbnailToWidth:(CGFloat)width height:(CGFloat)height {
    CGFloat currentWidth = self.thumbnailWidthConstraint.constant;
    CGFloat currentHeight = self.thumbnailHeightConstraint.constant;
    if (currentHeight != height || currentWidth != width) {
        self.thumbnailHeightConstraint.constant = height;
        self.thumbnailWidthConstraint.constant = width;
        [self.contentView setNeedsUpdateConstraints];
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.contentView setNeedsLayout];
    [self.contentView layoutIfNeeded];
}
@end

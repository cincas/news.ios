//
//  MainViewController.m
//  news.ios
//
//  Created by Tyler Yang on 16/2/4.
//  Copyright © 2016年 yangt. All rights reserved.
//

#import "MainViewController.h"
#import "MainViewModel.h"
#import "NewsAPIResponseItem.h"
#import "NewsItemTableViewCell.h"
@import AFNetworking;

static NSString * const kCellIdentifier = @"cellIdentifier";

@interface MainViewController () <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, weak) UITableView *tableView;
@property (nonatomic, weak) UIActivityIndicatorView *spinnerView;
@property (nonatomic, strong) NSMutableDictionary *imageDownloadStack;
@property (nonatomic, strong) NSMutableDictionary *imageStack;

@property (nonatomic, assign) BOOL useAFNetworking;
@end

@implementation MainViewController

- (instancetype)initWithViewModel:(MainViewModel *)viewModel {
    if (self = [self initWithNibName:nil bundle:nil]) {
        _viewModel = viewModel;
        _useAFNetworking = NO;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = self.viewModel.title;
    self.view.backgroundColor = UIColor.whiteColor;
    // Fixes for add UITableView as sub view to current view controller
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self prepareTableView];
    
    [self prepareSpinnerButton];
    [self prepareToggleButton];
    // Prepare content
    __weak MainViewController *_self = self;
    [self.spinnerView startAnimating];
    [self.viewModel prepareContent:^{
        [_self.tableView reloadData];
        [_self.spinnerView stopAnimating];
    } failed:^{
        // Nothing
    }];
}

- (void)prepareTableView {
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    tableView.translatesAutoresizingMaskIntoConstraints = NO;
    if ([tableView respondsToSelector:@selector(layoutMargins)]) {
        tableView.layoutMargins = UIEdgeInsetsZero;
        tableView.separatorInset = UIEdgeInsetsZero;
    }
    tableView.estimatedRowHeight = 44.f;
    tableView.rowHeight = UITableViewAutomaticDimension;
    tableView.delegate = self;
    tableView.dataSource = self;
    
    [tableView registerClass:[NewsItemTableViewCell class] forCellReuseIdentifier:kCellIdentifier];
    [self.view addSubview:tableView];
    self.tableView = tableView;
    
    NSLayoutConstraint *topConstraint = [NSLayoutConstraint constraintWithItem:tableView
                                                                  attribute:NSLayoutAttributeTop
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:self.topLayoutGuide
                                                                  attribute:NSLayoutAttributeTop
                                                                 multiplier:1
                                                                   constant:0];

    NSLayoutConstraint *leftConstraint = [NSLayoutConstraint constraintWithItem:tableView
                                                                     attribute:NSLayoutAttributeLeft
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.view
                                                                     attribute:NSLayoutAttributeLeft
                                                                    multiplier:1
                                                                      constant:0];
    
    NSLayoutConstraint *rightConstraint = [NSLayoutConstraint constraintWithItem:tableView
                                                                     attribute:NSLayoutAttributeRight
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.view
                                                                     attribute:NSLayoutAttributeRight
                                                                    multiplier:1
                                                                      constant:0];
    
    NSLayoutConstraint *bottomConstraint = [NSLayoutConstraint constraintWithItem:tableView
                                                                     attribute:NSLayoutAttributeBottom
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.bottomLayoutGuide
                                                                     attribute:NSLayoutAttributeBottom
                                                                    multiplier:1
                                                                      constant:0];
    
    [self.view addConstraints:@[topConstraint, leftConstraint, rightConstraint, bottomConstraint]];
}

#pragma mark - === UITableView datasource ===
#pragma mark -
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.viewModel.items.count;
}

- (NewsItemTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NewsItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(NewsItemTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NewsAPIResponseItem *item = [self.viewModel.items objectAtIndex:indexPath.row];
    if (item) {
        cell.titleLabel.text = item.title;
        cell.descriptionLabel.text = item.desc;
        [cell updateThumbnailToWidth:item.thumbnailImageWidth.floatValue
                              height:item.thumbnailImageHeight.floatValue];
        
        if (self.useAFNetworking) {
            [cell.thumbnailView setImageWithURL:item.thumbnailImageURL];
        } else {
            [self loadImageFromURL:item.thumbnailImageURL
                      imageView:cell.thumbnailView
                       indexPath:indexPath];
        }
    }
}

/**
 *  Ideally this part of logic should be moved to separate class/category,
 *  in order to perform unit test.
 *  Here is just a demonstration for how to handle asynchronized thread
 *
 */
#pragma mark - === Image download ===
#pragma mark -
- (NSMutableDictionary *)imageStack
{
    if (!_imageStack) {
        _imageStack = [NSMutableDictionary new];
    }
    return _imageStack;
}

- (void)loadImageFromURL:(NSURL *)imageURL imageView:(UIImageView *)imageView indexPath:(NSIndexPath *)indexPath {
    UIImage *image = self.imageStack[indexPath];
    if (!image) {
        [self downloadImageFromURL:imageURL
                         imageView:imageView
                         indexPath:indexPath];
    } else {
        imageView.image = image;
    }
}

- (void)downloadImageFromURL:(NSURL *)imageURL imageView:(UIImageView *)imageView indexPath:(NSIndexPath *)indexPath {
    
    if (!self.imageDownloadStack) {
        self.imageDownloadStack = [NSMutableDictionary new];
    }
    
    /**
     *  Because NSURLSessionTask is already using asynchronized way to send out request.
     *  no need to perform this in new queue like following code
     *  @code
     *  dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            //Background Thread
            dispatch_async(dispatch_get_main_queue(), ^(void){
                //Run UI Updates
            });
     *  });
     *
     */
    NSURLSessionTask *downloadTask = self.imageDownloadStack[indexPath];
    if (!downloadTask) {
        NSURLRequest *request = [NSURLRequest requestWithURL:imageURL];
        __weak MainViewController *_self = self;
        downloadTask = [[NSURLSession sharedSession] dataTaskWithRequest:request
                                                       completionHandler:^(NSData * _Nullable data,
                                                                           NSURLResponse * _Nullable response,
                                                                           NSError * _Nullable error) {
                                                           if (!error) {
                                                               UIImage *downloadImage = [UIImage imageWithData:data];
                                                               _self.imageStack[indexPath] = downloadImage;
                                                               [_self.imageDownloadStack removeObjectForKey:indexPath];
                                                               if (imageView) {
                                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                                       imageView.image = downloadImage;
                                                                   });
                                                               }
                                                           }
                                                       }];
        self.imageDownloadStack[indexPath] = downloadTask;
        [downloadTask resume];
    }
}

#pragma mark - === Demo methods ===
#pragma mark -
- (void)prepareSpinnerButton {
    UIActivityIndicatorView *spinnerView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    UIBarButtonItem *spinnerButton = [[UIBarButtonItem alloc] initWithCustomView:spinnerView];
    self.spinnerView = spinnerView;
    self.navigationItem.rightBarButtonItem = spinnerButton;
}

- (void)prepareToggleButton {
    UIBarButtonItem *toggleButton = [[UIBarButtonItem alloc] initWithTitle:@"Custom"
                                                                     style:UIBarButtonItemStylePlain
                                                                    target:self
                                                                    action:@selector(switchAFNetworking:)];
    toggleButton.possibleTitles = [NSSet setWithObjects:@"AFNetworking", @"Custom", nil];
    self.navigationItem.leftBarButtonItem = toggleButton;
}

- (void)switchAFNetworking:(UIBarButtonItem *)sender {
    self.useAFNetworking = !self.useAFNetworking;
    sender.title = self.useAFNetworking ? @"AFNetworking" : @"Custom";
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
}
@end

//
//  MainViewModel.m
//  news.ios
//
//  Created by Tyler Yang on 16/2/4.
//  Copyright © 2016年 yangt. All rights reserved.
//

#import "MainViewModel.h"
#import "MainViewController.h"
#import "NewsAPIClient.h"
#import "NewsAPIResponseList.h"

@interface MainViewModel ()
@property (nonatomic, strong) NewsAPIClient *apiClient;
@property (nonatomic, strong) NewsAPIResponseList *list;
@end

@implementation MainViewModel
- (instancetype) init {
    if (self = [super init]) {
        _title = @"Main view";
        _apiClient = [[NewsAPIClient alloc] initWithEndPoint:@"http://tcog.news.com.au/news/content/v1"];
    }
    return self;
}

#pragma mark - === Datasource methods ===
#pragma mark -
- (void)prepareContent:(void (^)())complete failed:(void (^)())failed
{
    MainViewModel *__weak _self = self;
    [_apiClient pullFrom:@"collection/7df8c106d7b8abccec7f9512177f62c5"
          withParameters:@{
                           @"product": @"tcog",
                           @"t_output": @"json",
                           @"pageSize": @(10)
                           }
                 success:^(NewsAPIResponseList *obj) {
                     _self.list = obj;
                     if (complete) {
                         complete();
                     }
                 }
                  failed:^(NSError *error) {
                      if (failed) {
                          failed();
                      }
                  }];
}

- (NSArray <NewsAPIResponseItem *> *)items
{
    if (self.list) {
        return self.list.items;
    }
    return nil;
}
@end

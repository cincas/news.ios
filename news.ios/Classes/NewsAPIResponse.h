//
//  NewsAPIResponse.h
//  news.ios
//
//  Created by Tyler Yang on 16/2/4.
//  Copyright © 2016年 yangt. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  Base class for api response model
 */
@interface NewsAPIResponse : NSObject
@property (nonatomic, copy, readonly) NSDictionary *responseBody; /**< Raw response (read-only) */

/**
 *  Init instance with given response body
 *
 *  @param responseBody Raw response from API
 *
 *  @return instancetype
 */
- (instancetype) initWithResponseBody:(NSDictionary *)responseBody;

@end

//
//  TYViewModel.h
//  news.ios
//
//  Created by Tyler Yang on 16/2/4.
//  Copyright © 2016年 yangt. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  Base ViewModel class
 */
@interface TYViewModel : NSObject
@end

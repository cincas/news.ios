//
//  NewsAPIResponse+Private.h
//  news.ios
//
//  Created by Tyler Yang on 16/2/4.
//  Copyright © 2016年 yangt. All rights reserved.
//

#ifndef NewsAPIResponse_Private_h
#define NewsAPIResponse_Private_h

#import "NewsAPIResponse.h"
static const NSString *kListItemsKey = @"results";

@interface NewsAPIResponse (Private)
/**
 *  Parse values from response body to instance 
 *  @throw Exception throw exception when response cannot be parsed
 */
- (void)parseResponseBody;
@end

#endif /* NewsAPIResponse_Private_h */

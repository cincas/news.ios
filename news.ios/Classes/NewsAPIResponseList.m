//
//  NewsAPIResponseList.m
//  news.ios
//
//  Created by Tyler Yang on 16/2/4.
//  Copyright © 2016年 yangt. All rights reserved.
//

#import "NewsAPIResponseList.h"
#import "NewsAPIResponse+Private.h"

#import "NewsAPIResponseItem.h"

@implementation NewsAPIResponseList
- (void)parseResponseBody
{
    _items = nil;
    if (self.responseBody) {
        id value = self.responseBody[kListItemsKey];
        if ([value isKindOfClass:[NSArray class]]) {
            // This can use NSValueTransformer
            NSArray <NSDictionary *> *items = (NSArray *)value;
            __block NSMutableArray *parsedItems = [[NSMutableArray alloc] initWithCapacity:items.count];
            [items enumerateObjectsUsingBlock:^(NSDictionary * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                @try {
                    NewsAPIResponseItem *parsedItem = [[NewsAPIResponseItem alloc] initWithResponseBody:obj];
                    [parsedItems addObject:parsedItem];
                }
                @catch (NSException *exception) {
                    // Handle exception from sub items
                }
                @finally {
                    // Do nothing
                }
            }];
            
            _items = parsedItems;
        }
    }
}
@end

//
//  NewsAPIClient.h
//  news.ios
//
//  Created by Tyler Yang on 16/2/4.
//  Copyright © 2016年 yangt. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NewsAPIResponse, AFHTTPSessionManager;
typedef void (^NewsAPISuccuessBlock) (__kindof NewsAPIResponse *obj);
typedef void (^NewsAPIFailedBlock) (NSError *error);

extern NSString * const kNewsAPIErrorDomain;

typedef NS_ENUM(NSUInteger, NewsAPIErrorCode) {
    NewsAPIErrorCodeInvalidURL = 0,
    NewsAPIErrorCodeNotFound = 404,
    NewsAPIErrorCodeInvalidResponse = 400
};

/**
 News API Client
 */
@interface NewsAPIClient : NSObject {
    AFHTTPSessionManager *_sessionManager;
}
/**
 *  Initialize APIClient with end point
 *
 *  @param urlString           API end point
 *
 *  @return NewsAPIClient   API client insatnce
 */
- (instancetype)initWithEndPoint:(NSString *)urlString;

/**
 *  Perform pull request from API, with optional parameters
 *
 *  @param url              End point
 *  @param params       Request parameters
 *
 */
- (void)pullFrom:(NSString *)url
  withParameters:(NSDictionary *)params
         success:(NewsAPISuccuessBlock)success
          failed:(NewsAPIFailedBlock)failed;
@end

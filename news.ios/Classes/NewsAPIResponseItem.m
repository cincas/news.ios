//
//  NewsAPIResponseItem.m
//  news.ios
//
//  Created by Tyler Yang on 16/2/4.
//  Copyright © 2016年 yangt. All rights reserved.
//

#import "NewsAPIResponseItem.h"
#import "NewsAPIResponse+Private.h"

@implementation NewsAPIResponseItem
- (void)parseResponseBody
{
    if (self.responseBody) {
        NSString *title = self.responseBody[@"title"];
        NSString *description = self.responseBody[@"description"];
        NSArray <NSString *> *authors = self.responseBody[@"authors"];
        NSDictionary *thumbnailImage = self.responseBody[@"thumbnailImage"];
        
        _title = title;
        _desc = description;
        _authors = authors;
        _thumbnailImage = thumbnailImage;
    }
}

- (NSURL *)thumbnailImageURL
{
    if (self.thumbnailImage && self.thumbnailImage[@"link"]) {
        return [NSURL URLWithString:self.thumbnailImage[@"link"]];
    }
    return nil;
}

- (NSNumber *)thumbnailImageWidth
{
    if (self.thumbnailImage) {
        return self.thumbnailImage[@"width"];
    }
    return nil;
}

- (NSNumber *)thumbnailImageHeight
{
    if (self.thumbnailImage)  {
        return self.thumbnailImage[@"height"];
    }
    return nil;
}
@end

//
//  NewsAPIResponseList.h
//  news.ios
//
//  Created by Tyler Yang on 16/2/4.
//  Copyright © 2016年 yangt. All rights reserved.
//

#import "NewsAPIResponse.h"

@class NewsAPIResponseItem;
/**
 *  List result model
 */
@interface NewsAPIResponseList : NewsAPIResponse
@property (nonatomic, copy, readonly) NSArray <NewsAPIResponseItem *> *items;
@end

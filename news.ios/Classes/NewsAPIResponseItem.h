//
//  NewsAPIResponseItem.h
//  news.ios
//
//  Created by Tyler Yang on 16/2/4.
//  Copyright © 2016年 yangt. All rights reserved.
//

#import "NewsAPIResponse.h"

/**
 *  Item model
 */
@interface NewsAPIResponseItem : NewsAPIResponse
@property (nonatomic, copy, readonly) NSString *title;
@property (nonatomic, copy, readonly) NSArray <NSString *> *authors;
@property (nonatomic, copy, readonly) NSDictionary *thumbnailImage;
@property (nonatomic, copy, readonly) NSString *desc;


/**
 *  Get NSURL for thumbnail image
 *
 *  @return NSURL thumbnail image url
 */
- (NSURL *)thumbnailImageURL;


/**
 *  Get width/height of thumbnail image
 *
 *  @return NSNumber width/height
 */
- (NSNumber *)thumbnailImageWidth;
- (NSNumber *)thumbnailImageHeight;
@end

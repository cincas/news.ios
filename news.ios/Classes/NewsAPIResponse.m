//
//  NewsAPIResponse.m
//  news.ios
//
//  Created by Tyler Yang on 16/2/4.
//  Copyright © 2016年 yangt. All rights reserved.
//

#import "NewsAPIResponse.h"
#import "NewsAPIResponse+Private.h"

@implementation NewsAPIResponse
- (instancetype)initWithResponseBody:(NSDictionary *)responseBody {
    if (self = [super init]) {
        _responseBody = responseBody;
        [self parseResponseBody];
    }
    return self;
}

#pragma mark -
#pragma mark - === Private methods ===
#pragma mark -
- (void)parseResponseBody
{
    // This method should be implemented by subclasses
}
@end

//
//  NewsAPIClient.m
//  news.ios
//
//  Created by Tyler Yang on 16/2/4.
//  Copyright © 2016年 yangt. All rights reserved.
//

#import "NewsAPIClient.h"
#import "NewsAPIResponseList.h"
@import AFNetworking;

NSString * const kNewsAPIErrorDomain = @"News.API.Error";

@interface NewsAPIClient ()
@property (nonatomic, strong) AFHTTPSessionManager *sessionManager;
@end

@implementation NewsAPIClient
- (instancetype)initWithEndPoint:(NSString *)urlString {
    if (self = [super init]) {
        NSURL *baseURL = [NSURL URLWithString:urlString];
        _sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
        
        // TCOG reture json response as text/plain
        NSSet *acceptableContentTypes = _sessionManager.responseSerializer.acceptableContentTypes;
        acceptableContentTypes = [acceptableContentTypes setByAddingObject:@"text/plain"];
        _sessionManager.responseSerializer.acceptableContentTypes = acceptableContentTypes;
    }
    return self;
}

- (void)pullFrom:(NSString *)url
  withParameters:(NSDictionary *)params
         success:(NewsAPISuccuessBlock)success
          failed:(NewsAPIFailedBlock)failed {
    [self.sessionManager GET:url
                  parameters:params
                     success:^(NSURLSessionDataTask *task, id responseObject) {
                         NewsAPIResponseList *listResponse;
                         if ([responseObject isKindOfClass:[NSDictionary class]]) {
                             NSDictionary *response = responseObject;
                             if (response[@"data"]) {
                                 listResponse = [[NewsAPIResponseList alloc] initWithResponseBody:response[@"data"]];
                             }
                         }
                         if (listResponse) {
                             if (success) {
                                success(listResponse);
                             }
                         } else {
                             NSError *error = [NSError errorWithDomain:kNewsAPIErrorDomain
                                                                  code:NewsAPIErrorCodeInvalidResponse
                                                              userInfo:@{
                                                                         @"url": task.originalRequest.URL,
                                                                         @"params": params ?: [NSNull null]
                                                                         }];
                             if (failed) {
                                 failed(error);
                             }
                         }
                     }
                     failure:^(NSURLSessionDataTask *task, NSError *error) {
                         NSLog(@"Error:%@", error);
                         if (failed) {
                             failed(error);
                         }
                     }];
    
}
@end

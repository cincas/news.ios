//
//  NewsAPIClient+XCTestExtensions.m
//  news.ios
//
//  Created by Tyler Yang on 16/2/4.
//  Copyright © 2016年 yangt. All rights reserved.
//

#import "NewsAPIClient+XCTestExtensions.h"

@interface NewsAPIClient ()
@end

@implementation NewsAPIClient (XCTestExtensions)
- (AFHTTPSessionManager *)sessionManager
{
    return _sessionManager;
}
@end

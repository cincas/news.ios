//
//  XCTestCase+Helper.h
//  news.ios
//
//  Created by Tyler Yang on 16/2/4.
//  Copyright © 2016年 yangt. All rights reserved.
//

#import <XCTest/XCTest.h>

@class NewsAPIResponseItem;
@interface XCTestCase (Helper)
- (void)validateResponse:(NSDictionary *)response withItem:(NewsAPIResponseItem *)item;
@end

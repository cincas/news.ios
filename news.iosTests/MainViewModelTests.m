//
//  MainViewModelTests.m
//  news.ios
//
//  Created by Tyler Yang on 16/2/9.
//  Copyright © 2016年 yangt. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MainViewModel.h"

@interface MainViewModelTests : XCTestCase

@end

@implementation MainViewModelTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testInitialize {
    MainViewModel *viewModel = [[MainViewModel alloc] init];
    XCTAssertNotNil(viewModel);
    
    NSString *expectedTitle = @"Main view";
    XCTAssertTrue([expectedTitle isEqualToString:viewModel.title]);
}

- (void)testPrepareContent {
    MainViewModel *viewModel = [[MainViewModel alloc] init];
    XCTestExpectation *successExpectation = [self expectationWithDescription:@"prepare content should success"];
    [viewModel prepareContent:^{
        [successExpectation fulfill];
    } failed:^{
        // this is the dead end.
    }];
    
    [self waitForExpectationsWithTimeout:30 handler:nil];
}
@end

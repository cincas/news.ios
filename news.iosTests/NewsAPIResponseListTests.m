//
//  NewsAPIResponseListTests.m
//  news.ios
//
//  Created by Tyler Yang on 16/2/4.
//  Copyright © 2016年 yangt. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "XCTestCase+Helper.h"

#import "NewsAPIResponse+Private.h"
#import "NewsAPIResponseList.h"
#import "NewsAPIResponseItem.h"

@interface NewsAPIResponseListTests : XCTestCase

@end

@implementation NewsAPIResponseListTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testParseResponseBodySuccess {
    NSMutableArray *itemsMock = [NSMutableArray arrayWithCapacity:5];
    NSInteger mockCount = 5;
    for (NSInteger i = 0 ; i < mockCount; i++) {
        NSDictionary *itemMock = [self createTestResponseItemWithIndex:i];
        [itemsMock addObject:itemMock];
    }
    
    NSDictionary *responseMock = @{
                                   kListItemsKey: itemsMock
                                   };
    
    NewsAPIResponseList *listResponse = [[NewsAPIResponseList alloc] initWithResponseBody:responseMock];
    XCTAssertTrue(listResponse.items.count == mockCount);
    [listResponse.items enumerateObjectsUsingBlock:^(NewsAPIResponseItem * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        XCTAssertTrue([obj isKindOfClass:[NewsAPIResponseItem class]]);
        NSDictionary *mockedItem = itemsMock[idx];
        XCTAssertNotNil(mockedItem);
        [self validateResponse:mockedItem withItem:obj];
    }];
}

- (NSDictionary *)createTestResponseItemWithIndex:(NSInteger)index {
    NSString *testTitle = [NSString stringWithFormat:@"test title %@", @(index)];
    NSString *testDescription = [NSString stringWithFormat:@"test description %@", @(index)];
    NSArray *testAuthors = @[
                             [NSString stringWithFormat:@"author 1 %@", @(index)],
                             [NSString stringWithFormat:@"author 2 %@", @(index)]
                             ];
    
    
    NSString *testThumbnailImageURL = [NSString stringWithFormat:@"http://www.test.com/path/to/image/%@", @(index)];
    NSDictionary *testThumbnailImage = @{
                                         @"link": testThumbnailImageURL
                                         };
    
    NSDictionary *validResponseBody = @{
                                        @"title": testTitle,
                                        @"description": testDescription,
                                        @"authors": testAuthors,
                                        @"thumbnailImage": testThumbnailImage
                                        };
    return validResponseBody;
}
@end

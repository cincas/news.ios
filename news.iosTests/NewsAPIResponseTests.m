//
//  NewsAPIResponseTests.m
//  news.ios
//
//  Created by Tyler Yang on 16/2/4.
//  Copyright © 2016年 yangt. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NewsAPIResponse.h"
#import "NewsAPIResponse+Private.h"

@interface NewsAPIResponseTests : XCTestCase

@end

@implementation NewsAPIResponseTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testInitialWithResponse
{
    NSDictionary *responseMock = @{};
    NewsAPIResponse *apiResponse = [[NewsAPIResponse alloc] initWithResponseBody:responseMock];
    XCTAssertNotNil(apiResponse);
}
@end

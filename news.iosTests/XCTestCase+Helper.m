//
//  XCTestCase+Helper.m
//  news.ios
//
//  Created by Tyler Yang on 16/2/4.
//  Copyright © 2016年 yangt. All rights reserved.
//

#import "XCTestCase+Helper.h"
#import "NewsAPIResponseItem.h"

@implementation XCTestCase (Helper)
- (void)validateResponse:(NSDictionary *)response withItem:(NewsAPIResponseItem *)item
{
    
    XCTAssertTrue([response[@"title"] isEqualToString:item.title]);
    XCTAssertTrue([response[@"description"] isEqualToString:item.desc]);
    XCTAssertTrue([response[@"authors"] isEqualToArray:item.authors]);
    XCTAssertTrue([response[@"thumbnailImage"] isEqualToDictionary:item.thumbnailImage]);
    
    NSURL *thumbnailImageURL = [NSURL URLWithString:[response valueForKeyPath:@"thumbnailImage.link"]];
    XCTAssertTrue([thumbnailImageURL.absoluteString isEqualToString:item.thumbnailImageURL.absoluteString]);
}
@end

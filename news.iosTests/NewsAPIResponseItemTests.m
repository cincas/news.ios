//
//  NewsAPIResponseItemTests.m
//  news.ios
//
//  Created by Tyler Yang on 16/2/4.
//  Copyright © 2016年 yangt. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "XCTestCase+Helper.h"

#import "NewsAPIResponseItem.h"
#import "NewsAPIResponse+Private.h"

@interface NewsAPIResponseItemTests : XCTestCase

@end

@implementation NewsAPIResponseItemTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testParseResponseBodySuccess {
    NSString *testTitle = @"test title";
    NSString *testDescription = @"test description";
    NSArray *testAuthors = @[ @"author 1", @"author 2"];
    NSString *testThumbnailImageURL = @"http://www.test.com/path/to/image";
    NSDictionary *testThumbnailImage = @{
                                         @"link": testThumbnailImageURL
                                         };
    
    NSDictionary *validResponseBody = @{
                                        @"title": testTitle,
                                        @"description": testDescription,
                                        @"authors": testAuthors,
                                        @"thumbnailImage": testThumbnailImage
                                        };
    
    NewsAPIResponseItem *itemResponse = [[NewsAPIResponseItem alloc] initWithResponseBody:validResponseBody];
    [self validateResponse:validResponseBody withItem:itemResponse];
}
@end

//
//  NewsAPIClientTests.m
//  news.ios
//
//  Created by Tyler Yang on 16/2/4.
//  Copyright © 2016年 yangt. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NewsAPIClient+XCTestExtensions.h"
#import "NewsAPIResponse.h"
@import AFNetworking;

@interface NewsAPIClientTests : XCTestCase

@end

@implementation NewsAPIClientTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testInitialize {
    NSString *endPoint = @"http://www.domain.com/path/to/api/version/";
    NewsAPIClient *apiClient = [[NewsAPIClient alloc] initWithEndPoint:endPoint];
    XCTAssertTrue([endPoint isEqualToString:apiClient.sessionManager.baseURL.absoluteString]);
}

- (void)testPullRequestFailed {
    XCTestExpectation *failedExpectation = [self expectationWithDescription:@"Request should failed"];
    NSString *endPoint = @"http://www.domain.com/path/to/api/version/";
    NewsAPIClient *client = [[NewsAPIClient alloc] initWithEndPoint:endPoint];
    NewsAPISuccuessBlock successBlock = ^(NewsAPIResponse *response) {
        XCTAssert([response isKindOfClass:[NewsAPIResponse class]]);
        // End expectation for now, wait sessionManager stub finish
        [failedExpectation fulfill];
    };
    NewsAPIFailedBlock failedBlock = ^(NSError *error) {
        //@TODO: test diffenret error domains
        [failedExpectation fulfill];
    };
    

    [client pullFrom:@"/content/endpoint" withParameters:nil success:successBlock failed:failedBlock];
    [self waitForExpectationsWithTimeout:30 handler:nil];
}

@end

//
//  NewsAPIClient+XCTestExtensions.h
//  news.ios
//
//  Created by Tyler Yang on 16/2/4.
//  Copyright © 2016年 yangt. All rights reserved.
//

#import "NewsAPIClient.h"

@class AFHTTPSessionManager;
@interface NewsAPIClient (XCTestExtensions)
/**
 *@TODO: Stub sessionManager for Unit Test
 */
- (AFHTTPSessionManager *)sessionManager;
@end
